//
//  Navigator.swift
//  Displayer
//
//  Created by Muhammad Fahied on 11/4/17.
//  Copyright © 2017 Muhammad Fahied. All rights reserved.
//

import UIKit

class Navigator {
    
    let navigationController : UINavigationController
    
    init(window: UIWindow) {
    
        navigationController = UINavigationController()
        
        // Allocate memory for an instance of the 'HomeViewController' class
        let homeViewController = HomeViewController()
        homeViewController.showItems = self.showItems

        // Set root view controller for navigation controller
        navigationController.setViewControllers([homeViewController], animated: false)
        
        // Set the root view controller of the app's window
        window.rootViewController = navigationController
    }
    
    
    func showItems() {
        
        let storageService = StorageService()
        
        let itemViewModel = ItemViewModel(storageService: storageService)
        
        let itemsVC = ItemsViewController()
        itemsVC.viewModel = itemViewModel
        itemsVC.itemSelected = showDetails
        navigationController.show(itemsVC, sender: nil)
    }
    
    func showDetails(item: Item) -> () {
        let detailVC = DetailViewController()
        detailVC.item = item
        navigationController.show(detailVC, sender: nil)
    }
    
}
