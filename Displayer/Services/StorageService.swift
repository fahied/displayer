//
//  StorageService.swift
//  Displayer
//
//  Created by Muhammad Fahied on 11/3/17.
//  Copyright © 2017 Muhammad Fahied. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxRealm

struct StorageService: StorageServiceType {

    
    fileprivate func withRealm<T>(_ operation: String, action: (Realm) throws -> T) -> T? {
        do {
            let realm = try Realm()
            return try action(realm)
        } catch let err {
            print("Failed \(operation) realm with error: \(err)")
            return nil
        }
    }
    
    @discardableResult
    func saveItem(item: Item) -> Observable<Item> {
        let result = withRealm("creating") { realm -> Observable<Item> in
            try realm.write {
                realm.add(item)
            }
            return .just(item)
        }
        return result ?? .error(StorageServiceError.creationFailed)
    }
    
    @discardableResult
    func saveItems(items: [Item]) -> Observable<[Item]> {
        let result = withRealm("creating") { realm -> Observable<[Item]> in
            try realm.write {
                realm.add(items, update: true)
            }
            return .empty()
        }
        return result ?? .error(StorageServiceError.creationFailed)
    }
        
    @discardableResult
    func delete(item: Item) -> Observable<Void> {
        let result = withRealm("deleting") { realm-> Observable<Void> in
            try realm.write {
                realm.delete(item)
            }
            return .empty()
        }
        return result ?? .error(StorageServiceError.deletionFailed(item))
    }
    
    @discardableResult
    func update(item: Item, title: String) -> Observable<Item> {
        let result = withRealm("updating title") { realm -> Observable<Item> in
            try realm.write {
                item.title = title
            }
            return .just(item)
        }
        return result ?? .error(StorageServiceError.updateFailed(item))
    }
    
    @discardableResult
    func toggleFavorite(item: Item) -> Observable<Item> {
        let result = withRealm("toggling") { realm -> Observable<Item> in
            try realm.write {
                if item.favorited == nil {
                    item.favorited = Date()
                } else {
                    item.favorited = nil
                }
            }
            return .just(item)
        }
        return result ?? .error(StorageServiceError.toggleFailed(item))
    }
    
    @discardableResult
    func items() -> Observable<Results<Item>> {
        let result = withRealm("getting Items") { realm -> Observable<Results<Item>> in
            let realm = try Realm()
            let items = realm.objects(Item.self)
            
            // if there are no items in the Realm then download it
            // this code has to be refactored later
            if items.count == 0 {
                //fetch data from backend
                BackendApi.sharedInstance.fetchItemList()
            }
            return Observable.collection(from: items)
        }
        return result ?? .empty()
    }
}
