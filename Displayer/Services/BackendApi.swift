//
//  APIService.swift
//  Displayer
//
//  Created by Muhammad Fahied on 11/3/17.
//  Copyright © 2017 Muhammad Fahied. All rights reserved.
//

import Alamofire
import UnboxedAlamofire
import RealmSwift

class BackendApi {
    
    static let sharedInstance = BackendApi()
    
    // MARK: - API Addresses
    fileprivate enum Address: String {
        case channelsList = "/ams/v3/getChannelList"
        case channels = "/ams/v3/getChannels"
        case events = "/ams/v3/getEvents"
        
        private var baseURL: String { return "http://ams-api.astro.com.my" }
        var url: URL {
            return URL(string: baseURL.appending(rawValue))!
        }
    }
    
    func fetchItemList() {
        
        let url = Address.channelsList.url
        Alamofire.request(url, method: .get).responseArray(keyPath: "channels") { (response: DataResponse<[Item]>) in
            // handle response
            if let channels = response.result.value {
                StorageService().saveItems(items: channels)
            }
            // handle error
            if let error = response.result.error as? UnboxedAlamofireError {
                print("error: \(error.description)")
            }
        }
    }
}
