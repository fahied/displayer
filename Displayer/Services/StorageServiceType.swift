//
//  StorageServiceType.swift
//  Displayer
//
//  Created by Muhammad Fahied on 11/3/17.
//  Copyright © 2017 Muhammad Fahied. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

enum StorageServiceError: Error {
    case creationFailed
    case updateFailed(Item)
    case deletionFailed(Item)
    case toggleFailed(Item)
}

protocol StorageServiceType {
    @discardableResult
    func saveItem(item: Item) -> Observable<Item>

    @discardableResult
    func saveItems(items: [Item]) -> Observable<[Item]>
    
    @discardableResult
    func delete(item: Item) -> Observable<Void>
    
    @discardableResult
    func update(item: Item, title: String) -> Observable<Item>
    
    @discardableResult
    func toggleFavorite(item: Item) -> Observable<Item>
    
    func items() -> Observable<Results<Item>>
}
