//
//  Item.swift
//  Displayer
//
//  Created by Muhammad Fahied on 11/2/17.
//  Copyright © 2017 Muhammad Fahied. All rights reserved.
//

import Unbox
import RealmSwift
import RxDataSources

class Item: Object, Unboxable {
    
    // Properties
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var number: Int = 0
    @objc dynamic var favorited: Date?
    
    // Unbox object
    required convenience init(unboxer: Unboxer) throws {
     self.init()
        self.id = try unboxer.unbox(key: "channelId")
        self.title = try unboxer.unbox(key: "channelTitle")
        self.number = try unboxer.unbox(key: "channelStbNumber")
    }
    
    // unique identifier for record
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Item: IdentifiableType {
    var identity: Int {
        return self.isInvalidated ? 0 : id
    }
}
