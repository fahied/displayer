//
//  ItemGridCell.swift
//  Displayer
//
//  Created by Muhammad Fahied on 11/2/17.
//  Copyright © 2017 Muhammad Fahied. All rights reserved.
//

import UIKit
import Action
import RxSwift

class ItemTableViewCell: UITableViewCell {
    
    var disposeBag = DisposeBag()
    
    ///  Struct for configuration and customizations
    struct Config {
        
        // Image to unfavorite
        public static var unfovoriteImage        : UIImage = #imageLiteral(resourceName: "red-favorite-icon")
        // Image to favorite
        public static var fovoriteImage        : UIImage = #imageLiteral(resourceName: "gray-favorite-icon")
        //Font for number label
        public static var numberfont             :   UIFont  = UIFont.boldSystemFont(ofSize: 18.0)
        //Font for title label
        public static var titlefont              :   UIFont  = UIFont.systemFont(ofSize: 16.0)
    }
    
    /// Lazy var for info title
    open fileprivate(set) lazy var numberLabel: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor           = UIColor.clear
        label.textAlignment             = NSTextAlignment.center
        label.font                      = Config.numberfont
        return label
    }()
    
    /// Lazy var for info title
    open fileprivate(set) lazy var titleLabel: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor           = UIColor.clear
        label.textAlignment             = NSTextAlignment.left
        label.numberOfLines             = 0
        label.font                      = Config.titlefont
        return label
    }()
    
    /// Lazy var for favorite button
    open fileprivate(set) lazy var favoriteButton: UIButton = {
        
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createViewsAndSetConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createViewsAndSetConstraints()
    }
    
    func configure(with item: Item, action: CocoaAction) {
        
        favoriteButton.rx.action = action
        
        item.rx.observe(Int.self, "number")
            .subscribe(onNext: { [weak self] number in
                self?.numberLabel.text = "\(number ?? 0)"
            })
            .disposed(by: disposeBag)
        
        item.rx.observe(String.self, "title")
            .subscribe(onNext: { [weak self] title in
                self?.titleLabel.text = title
            })
            .disposed(by: disposeBag)
        
        item.rx.observe(Date.self, "favorited")
            .subscribe(onNext: { [weak self] date in
                let image = date == nil ? Config.fovoriteImage : Config.unfovoriteImage
                self?.favoriteButton.setImage(image, for: .normal)
            })
            .disposed(by: disposeBag)
        
    }
    
    override func prepareForReuse() {
        disposeBag = DisposeBag()
        super.prepareForReuse()
    }
    
    class func reuseIdentifier() -> String {
        return "ItemTableViewCell"
    }
}

extension ItemTableViewCell {
    
    /// Function to build a views and set constraint
    func createViewsAndSetConstraints(){
        
        //Add subviews to current view
        [numberLabel, titleLabel, favoriteButton].forEach({contentView.addSubview($0)})
        
        //autolayout the stack view and elements
        let viewsDictionary = [
            "numberLabel":numberLabel,
            "titleLabel" : titleLabel,
            "favoriteButton": favoriteButton
            ] as [String : Any]
        
        let metrics = ["paddingV": 8, "paddingH": 8]
        
        var allConstraints = [NSLayoutConstraint]()
        
        //constraint for
        let containerHC = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-(paddingH)-[numberLabel(60)]-(paddingH)-[titleLabel]-(>=paddingH)-[favoriteButton(35)]-(paddingH)-|",
            options: NSLayoutFormatOptions(rawValue: 0),
            metrics: metrics,
            views: viewsDictionary
        )
        allConstraints += containerHC
        
        //constraint for number label
        
        viewsDictionary.forEach({ key, value in
          
            let viewVC = NSLayoutConstraint.constraints(
                withVisualFormat: "V:|-(paddingV)-[\(key)]-(paddingV)-|",
                options: NSLayoutFormatOptions(rawValue:0),
                metrics: metrics,
                views: viewsDictionary
            )
            allConstraints += viewVC
        })
        
        NSLayoutConstraint.activate(allConstraints)
    }
}


