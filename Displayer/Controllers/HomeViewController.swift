//
//  ViewController.swift
//  Displayer
//
//  Created by Muhammad Fahied on 11/2/17.
//  Copyright © 2017 Muhammad Fahied. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    // Home button action
    public var showItems: () -> () = {}
    
    fileprivate let button = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //set title
        title = "Home"
        button.frame = CGRect(x: 100, y: 100, width: 200, height: 40)
        button.isEnabled = true
        button.setTitle("Show Channels List", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.addTarget(self, action: #selector(tapButton), for: .touchUpInside)
       view.addSubview(button)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func tapButton() {
        self.showItems()
    }
}

