//
//  ListViewController.swift
//  Displayer
//
//  Created by Muhammad Fahied on 11/2/17.
//  Copyright © 2017 Muhammad Fahied. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import Action
import NSObject_Rx

class ItemsViewController: UIViewController {
    // outlet
    var itemSelected: (Item) -> () = { _ in}
    // properties
    fileprivate var tableView = UITableView()
    var viewModel: ItemViewModel!
    let dataSource = RxTableViewSectionedAnimatedDataSource<ItemSection>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set title
        title = "Channels"

        // frame based UI
        let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
        tableView.frame = CGRect(x: 0, y: navigationBarHeight, width: self.view.frame.width, height: self.view.frame.height - navigationBarHeight)
        self.view.addSubview(tableView)
        
        tableView.register(ItemTableViewCell.self, forCellReuseIdentifier: ItemTableViewCell.reuseIdentifier())
        
        configureDataSource()
        bindViewModel()
    }

    func bindViewModel() {
        
        viewModel.sectionedItems
            .bind(to: (tableView.rx.items(dataSource: dataSource)))
            .disposed(by: self.rx_disposeBag)

        tableView.rx.itemSelected.subscribe(onNext: {(indexPath) in
            //
            let item = try! self.dataSource.model(at: indexPath) as! Item
            self.itemSelected(item)
        }).disposed(by: self.rx_disposeBag)
    }
    
    fileprivate func configureDataSource() {
        dataSource.titleForHeaderInSection = { dataSource, index in
            dataSource.sectionModels[index].model
        }
        
        dataSource.configureCell = { [weak self]
            dataSource, tableView, indexPath, item in
            let cell = tableView.dequeueReusableCell(withIdentifier:ItemTableViewCell.reuseIdentifier(), for: indexPath) as! ItemTableViewCell
            if let strongSelf = self {
                cell.configure(with: item, action: strongSelf.viewModel.onFavorite(item: item))
            }
            return cell
        }
    }
}
