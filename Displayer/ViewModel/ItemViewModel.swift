//
//  ItemViewModel.swift
//  Displayer
//
//  Created by Muhammad Fahied on 11/3/17.
//  Copyright © 2017 Muhammad Fahied. All rights reserved.
//


import Foundation
import RxSwift
import RxDataSources
import Action

typealias ItemSection = AnimatableSectionModel<String, Item>

struct ItemViewModel {
    
    let storageService: StorageServiceType
    
    // Default: Sort items by title
    public var sortKey = "title"
    public var sortascending:Bool = true
    
    init(storageService: StorageServiceType) {
        self.storageService = storageService
    }
    
    func onFavorite(item: Item) -> CocoaAction {
        return CocoaAction {
            return self.storageService.toggleFavorite(item: item).map { _ in }
        }
    }
    
    var sectionedItems: Observable<[ItemSection]> {
        return self.storageService.items()
            .map { results in
                let favoriteItems = results
                    .filter("favorited != nil")
                    .sorted(byKeyPath: self.sortKey, ascending: true)
                
                let items = results
                    .filter("favorited == nil")
                    .sorted(byKeyPath: self.sortKey, ascending: true)
                
                return [
                    ItemSection(model: "Favorited Items", items: favoriteItems.toArray()),
                    ItemSection(model: "Items", items: items.toArray())
                ]
        }
    }
}

