# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Tools and Technologies Used in this project ###

* MVVM - Architectural Pattern
* Reactive Programming
* RxSwift
* RxCocoa
* Action
* Auto Layout & Frames
* Protocols oriented Programming
* REST api calls
* Alamofire
* Unbox
* Unboxed+Alamofire
* RxRealm - Persistance
* RxDataSources
* CocoaPods


### How do I get set up? ###

* pod install
* open the workspace


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact